Rails.application.routes.draw do
	root 'home#index'

	get '/about', to: 'home#about'
	get '/portfolio', to: 'home#portfolio'
	get '/contact', to: 'home#contact'
	post '/send_contact', to: 'home#send_contact'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
