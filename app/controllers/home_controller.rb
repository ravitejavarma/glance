class HomeController < ApplicationController
	skip_before_action :verify_authenticity_token

	def index
	end

	def about
	end

	def portfolio
	end

	def contact
	end

	def send_contact
		if params[:name].present? and params[:mobile].present? and params[:message].present?
			ContactMailer.contact(params[:name], params[:mobile], params[:email], params[:subject], params[:message]).deliver_now
			render status: 200, json: {'message' => 'Success'} and return
		else
			render status: 401, json: {'message' => 'Params missing'} and return
		end
	end
end
