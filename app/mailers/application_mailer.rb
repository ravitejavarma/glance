class ApplicationMailer < ActionMailer::Base
  default from: 'developer.glance@gmail.com'
  layout 'mailer'
end
