class ContactMailer < ApplicationMailer

  def contact(name, mobile, email, subject, message)
    @name = name
    @mobile = mobile
    @email = email
    @subject = subject
    @message = message
    mail(to: 'developer.glance@gmail.com',
    subject: "Contact Us Form",
    template: "contact"
    )
  end

end
